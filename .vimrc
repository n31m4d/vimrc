"load pathogen plugin
filetype off
call pathogen#infect()
filetype plugin indent on

" we don't need to be compatible with the old vi
set nocompatible

" avoid security exploit with modeline
set modelines=0

" tab settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
" convert tabs to spaces
" set expandtab

"we override the general tab settings for YAML files, setting the tab as
"2 spaces
autocmd BufRead,BufNewFile *.yml setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab

set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set relativenumber
" Create an undo file, so that you can undo even after closing and reopening
"set undofile

" set leader key
let mapleader = ","

" searching/moving
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %

" long lines
set wrap
set textwidth=79
set formatoptions=qrn1
set colorcolumn=85

" invisible characters
"set list
"set listchars=tab:?\ ,eol:?

" ignore help key
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

" save on losing focus
au FocusLost * :wa

" set color
syntax on

" set 256 colors
set t_Co=256

" colorscheme
colorscheme molokai

" emmet tab
" imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")
let user_emmet_expandabbr_key = '<c-e>'
